d7uswds Make
=========

d7uswds Drupal make file

v7.x-1.0

d7uswds is a full D7 install profile, based on US Web Design Standards v1.0. It provides a complete Drupal setup & basic config to reduce time spent doing repetitive install & setup tasks common to all Drupal 7 sites.

It includes a comprehensive set of modules alongside a default admin theme (Adminimal) & base theme (wdsd)

Docs:
----

Run the following commands from commandline (unix/linux) **inside the directory you will want to create the new site**.

**Note:** [Drush](http://docs.drush.org/en/master/install/) needs to be installed on your machine!
``````
git clone https://gitlab.com/d7uswds/d7uswds-make.git
cd d7uswds-make
mv drupal-org-core.make ../
cd ..
rm -rf d7uswds-make
drush make drupal-org-core.make
``````


One-line command ;)

`git clone https://gitlab.com/d7uswds/d7uswds-make.git && cd d7uswds-make && mv drupal-org-core.make ../ && cd .. && rm -rf d7uswds-make && drush make drupal-org-core.make`

**NOTE** You should create a local DB for Drupal. You could use phpmyadmin or command line in linux:

``````
mysql> create database dbname; GRANT ALL PRIVILEGES ON dbname.* To 'username'@'host' IDENTIFIED BY 'superstrongpassword'; FLUSH PRIVILEGES;
``````

