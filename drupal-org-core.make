; d7wds_profile - Install profile
; v7.x-2.0

api = 2
core = 7.x

; If patches are needed
; ---------
; projects[drupal][patch][] = "http://cgit.drupalcode.org/ais/plain/ais.htaccess.patch"

;--------------------------------------------/
; Buckup Migrate php7 compat
; Discussion and patch: https://www.drupal.org/node/2623598 php7 compat
;--------------------------------------------/
projects[backup_migrate][version] = 3.1
projects[backup_migrate][patch][2623598] = "https://www.drupal.org/files/issues/backup_migrate-fix_constructor_for_future_version_php-2623598-5-7.x.patch"

;--------------------------------------------/
; field_groups php7 compat
; Discussion and patch: https://www.drupal.org/node/2649648
;--------------------------------------------/
projects[field_group][version] = 1.5
projects[field_group][patch][2649648] = "https://www.drupal.org/files/issues/php7_uniform_variable-2649648-5.patch"

;--------------------------------------------/
; Interface to remove url redirects from Entities
; Discussion and patch here: https://www.drupal.org/node/2403891
;------------/
projects[redirect][version] = 1.0-rc3
projects[redirect][patch][2403891] = "https://www.drupal.org/files/issues/redirect-entity_types-2403891-10.patch"

;--------------------------------------------/
; Patched version of simple_toc module. To adapt indentation and nesting and also adding
; mbstring mod checks
;------------/
projects[simple_toc][version] = 1.0
projects[simple_toc][patch][] = "https://gitlab.com/d7uswds/simple_toc_patch/raw/master/simple_toc-nesting_changes_and_mbstring_check.patch"

;--------------------------------------------/
; Adminimal Custom css is needed to correctly display color module table.
; The css should be under git control: https://www.drupal.org/node/2639508
;------------/
projects[adminimal_theme][version] = 1.24
projects[adminimal_theme][patch][2639508] = "https://www.drupal.org/files/issues/2639508--adminimal-custom-css-3.patch"

;--------------------------------------------/
; UUID module pre build alter
; https://www.drupal.org/node/2308983
;------------/
projects[uuid][version] = 1.x-dev
projects[uuid][patch][2308983] = "https://www.drupal.org/files/issues/uuid-hook_uuid_entities_pre_rebuild_alter-2308983-1.patch"

;--------------------------------------------/
; Paragraphs Add UUID module support
; https://www.drupal.org/node/2471174
;------------/
projects[paragraphs][version] = 1.x-dev
projects[paragraphs][patch][2471174] = "https://www.drupal.org/files/issues/paragraphs-add_uuid_support-2471174-17.patch"


projects[] = drupal

; Projects
; --------
;projects[] = adminimal_theme
projects[] = administerusersbyrole
projects[] = admin_menu
projects[] = admin_views
projects[] = bean
projects[] = better_exposed_filters
projects[] = breakpoints
projects[] = chain_menu_access
projects[] = ckeditor
projects[] = ckeditor_link
projects[] = ctools
projects[] = date
projects[] = devel
projects[] = entity
projects[] = entityreference
projects[] = entity_dependency
projects[] = features
projects[] = features_extra
projects[] = field_formatter_settings
projects[] = field_tools
projects[] = google_analytics
projects[] = hide_formats
projects[] = image
projects[] = inline_entity_form
projects[] = jquery_update
projects[] = libraries
projects[] = link
projects[] = menu_admin_per_menu
projects[] = menu_attributes
projects[] = menu_block
projects[] = menu_position
projects[] = metatag
projects[] = module_filter
projects[] = multiupload_filefield_widget
projects[] = multiupload_imagefield_widget
projects[] = panels
projects[] = pathauto
projects[] = permission_select
;projects[] = paragraphs
projects[] = picture
projects[] = publishcontent
projects[] = rename_admin_paths
projects[] = role_delegation
projects[] = schemaorg
projects[] = site_map
projects[] = smart_trim
projects[] = stage_file_proxy
projects[] = strongarm
projects[] = token
projects[] = views
projects[] = views_bulk_operations
projects[] = view_unpublished
projects[] = xmlsitemap
projects[] = diff
projects[] = email
projects[] = menu_token
;projects[] = semantic_panels
projects[style_settings] = 2.0
projects[] = telephone
;projects[] = uuid
projects[] = uuid_features
projects[] = node_export

; Dev modules
; --------
;projects[semantic_panels][version] = "1.x-dev"
;projects[semantic_panels][download][type] = "git"
;projects[semantic_panels][download][url] = "git://git.drupal.org/project/semantic_panels.git"
;projects[semantic_panels][download][revision] = "0bd3b59a2ac9f91ec6ddf5b3ada2c9073dbc51d3"


; Custom modules
; --------
projects[d7uswds_tweaks][type] = "module"
projects[d7uswds_tweaks][download][type] = "git"
projects[d7uswds_tweaks][download][url] = "https://gitlab.com/d7uswds/d7uswds_tweaks.git"
projects[d7uswds_tweaks][subdir] = "custom"

; Profiles
; --------
projects[d7uswds_profile][type] = "profile"
projects[d7uswds_profile][download][type] = "git"
projects[d7uswds_profile][download][url] = "https://gitlab.com/d7uswds/d7uswds_profile.git"
projects[d7uswds_profile][subdir] = ""
projects[d7uswds_profile][destination] = "profiles"

;projects[d7uswds_profile][type] = "profile"
;projects[d7uswds_profile][download][type] = "file"
;projects[d7uswds_profile][download][url] = "/home/mariana/mobomo/Nasa/profiles/test_local/d7uswds_profile_latest.tar"
;projects[d7uswds_profile][subdir] = ""
;projects[d7uswds_profile][destination] = "profiles"

; Themes
; --------
projects[drubath][type] = "theme"
projects[drubath][download][type] = "git"
projects[drubath][download][url] = "https://gitlab.com/drofile/drubath.git"
projects[drubath][subdir] = ""
projects[drubath][destination] = "themes"

projects[wdsd][type] = "theme"
projects[wdsd][download][type] = "git"
projects[wdsd][download][url] = "https://gitlab.com/d7uswds/wdsd.git"
projects[wdsd][subdir] = ""
projects[wdsd][destination] = "themes"

defaults[projects][subdir] = "contrib"

; Libraries
; ---------

; CKEditor
libraries[ckeditor][download][type]= "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.4/ckeditor_4.4.4_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"
